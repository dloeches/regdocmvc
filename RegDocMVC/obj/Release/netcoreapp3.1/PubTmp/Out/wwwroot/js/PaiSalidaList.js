﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});


function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        //"processing": true,
        //"serverSide": true,
        "filter": true,
        "ajax": {
            "url": "PAI_SALIDA/GetAll/",
            "type": "GET",
            "datatype": "json"
        },
        "order": [[1, "desc"]], //or asc 
        //"columnDefs": [{
        //    "targets": [0],
        //    "visible": false,
        //    "searchable": false
        //}],
        "columns": [
            { "data": "codPai", "width": "8%" },
            { "data": "nReg", "width": "12%" },
            {
                "data": "fEntrada", "width": "8%", "type": "date", "render": function (d) {
                    return moment(d).format("DD/MM/YYYY");
                }
            },
            { "data": "propietarioName", "width": "10%" },
            { "data": "areaDocName", "width": "10%" },
            { "data": "tipoDocName", "width": "10%" },
            { "data": "referencia", "width": "10%" },
            { "data": "tipologiaName", "width": "10%" },
            {
                "data": "nReg",
                "render": function (data) {
                    return `<div class="text-center">
                        <a href="PAI_SALIDA/Upsert?id=${data}" class='btn btn-success text-white' style='cursor:pointer; width:50px;'>
                           <i class="far fa-file"></i>
                        </a>
                        </div>`;
                }, "width": "20%"
                 //&nbsp;
                 //<a class='btn btn-danger text-white' style='cursor:pointer; width:70px;'
                 //    onclick=Delete('PAI_SALIDA/Delete?id='+${data})>
                 //    Borrar
                 //</a>
            }
        ],
        "language": {
            "emptyTable": "no hay datos",
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "width": "100%"
    });
}

function Delete(url) {
    swal({
        title: "Estas seguro?",
        text: "Una vez borrado, no se podrá recuperar",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}