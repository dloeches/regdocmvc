﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});


function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        "ajax": {
            "url": "PAI_INFORM/GetAll/",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "codPai",   "width": "10%" },
            { "data": "nombre", "width": "20%" },
            { "data": "poblacion", "width": "20%" },
            { "data": "unidEjec", "width": "20%" },
            { "data": "provincia", "width": "10%" },
            {
                "data": "numPai",
                "render": function (data) {
                    return `<div class="text-center">
                        <a href="PAI_INFORM/Upsert?id=${data}" class='btn btn-success text-white' style='cursor:pointer; width:50px;'>
                           <i class="far fa-file"></i>
                        </a>
                        </div>`;
                }, "width": "20%"
                           //&nbsp;
                        //<a class='btn btn-danger text-white' style='cursor:pointer; width:70px;'
                        //    onclick=Delete('PAI_INFORM/Delete?id='+${data})>
                        //    Borar
                        //</a>
            }
        ],
        "language": {
            "processing": "Cargando Datos...",  
            "emptyTable": "no hay datos",
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "width": "100%"
    });
}

function Delete(url) {
    swal({
        title: "Estas seguro?",
        text: "Una vez borrado, no se podrá recuperar",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}