﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});

function loadDataTable() {
    dataTable = $('#DT_load').DataTable({
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'lfBrtip',
        //buttons: [
        //    'copy', 'excel', 'pdf'
        //],

        buttons: [
            {
                extend: 'copy',
                className: 'btn btn-dark rounded-0',
                text: '<i class="far fa-copy"></i> Copy'
            },
            //{
            //    extend: 'excel',
            //    className: 'btn btn-dark rounded-0',
            //    text: '<i class="far fa-file-excel"></i> Excel'
            //},
            {
                extend: 'pdf',
                className: 'btn btn-dark rounded-0',
                text: '<i class="far fa-file-pdf"></i> Pdf'
            },
            {
                extend: 'csv',
                className: 'btn btn-dark rounded-0',
                text: '<i class="fas fa-file-csv"></i> CSV',
               
               // exportOptions: { modifier: { page: 'all' } }
            },
            //{
            //    extend: 'print',
            //    className: 'btn btn-dark rounded-0',
            //    text: '<i class="fas fa-print"></i> Print'
            //}
        ] ,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "PAI_ENTRADA/GetRegistrosEntrada/",
            "type": "POST",
            "datatype": "json"
        },
        "order": [[1, "desc"]], //or asc 
        //  "columnDefs": [{
        //    "targets": [0],
        //    "visible": false,
        //    "searchable": false
        //}],
        "columns": [
            { "data": "codPai", "name": "codPai", "width": "8%" },
            { "data": "nReg", "name": "nReg", "width": "12%" },
            {
                "data": "fEntrada", "name": "fEntrada", "width": "8%", "type": "date", "render": function (d) {
                    return moment(d).format("DD/MM/YYYY");
                }
            }, 
            { "data": "propPai", "name": "propPai", "width": "10%" },
            { "data": "areaDoc", "name": "areaDoc", "width": "10%" },
            { "data": "tipoDoc", "name": "tipoDoc","width": "10%" },
            { "data": "referencia", "name": "referencia","width": "10%" },
            { "data": "tipologia", "name": "tipologia","width": "10%" },
            {
                "data": "nReg",
                "bSortable": false,
                "render": function (data) {
                    return `<div class="text-center">
                        <a href="PAI_ENTRADA/Upsert?id=${data}" class='btn btn-success text-white' style='cursor:pointer; width:50px;'>
                           <i class="fas fa-edit"></i>
                        </a>
                        <a class='btn btn-danger text-white' style='cursor:pointer;width:50px;margin-left:6px' onclick=Delete('PAI_ENTRADA/Delete?id='+${data})>
                           <i class="fas fa-trash-alt"></i>
                        </a>
                        </div>`;
                }, "width": "20%"
            }
        ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json",
            "search": "Buscar por todas las columnas",
            "emptyTable": "no hay datos"
        },
        "width": "100%"
    });

    //Apply Custom search on jQuery DataTables here
    oTable = $('#DT_load').DataTable();
    $('#btnFilter').click(function () {
        //Apply search for código // DataTable column index 0
        oTable.columns(0).search($('#txtCodigo').val().trim());

        //Apply search for Nreg // DataTable column index 1
        oTable.columns(1).search($('#txtNReg').val().trim());

        //Apply search for fEntrada // DataTable column index 2
        oTable.columns(2).search($('#txtFEntradaIni').val().trim());

        //Apply search for propPai // DataTable column index 3
        oTable.columns(3).search($('#ddPropietario').val().trim());

        //Apply search for areaDoc // DataTable column index 4
        oTable.columns(4).search($('#ddAreaDocs').val().trim());

        //Apply search for tipoDoc // DataTable column index 5
        oTable.columns(5).search($('#ddTipoDocs').val().trim());

        //Apply search for referencia // DataTable column index 6
        oTable.columns(6).search($('#txtReferencia').val().trim());

        //Apply search for tipologia // DataTable column index 7
        oTable.columns(7).search($('#ddTipologia').val().trim());

        oTable.columns(8).search($('#txtFEntradaFin').val().trim());

        //hit search on server
        oTable.draw();
    });
}

function Delete(url) {
    swal({
        title: "Estas seguro?",
        text: "Una vez borrado, no se podrá recuperar",
        icon: "warning",
        buttons: true,
        dangerMode: true
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: "DELETE",
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        }
    });
}