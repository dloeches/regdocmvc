﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegDocMVC.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<PaiInform> PAI_INFORM { get; set; }
        public DbSet<PaiEntrada> PAI_ENTRADA { get; set; }
        public DbSet<PaiSalida> PAI_SALIDA { get; set; }
        public DbSet<PaiTipoDoc> PAI_TIPODOC{ get; set; }
        public DbSet<PaiAreaDoc> PAI_AREADOC { get; set; }
        public DbSet<PaiEntidadPropietario> PAI_PROPENTIDAD{ get; set; }
        public DbSet<PaiTipologia> PAI_TIPOLOGIA { get; set; }
    }
}
