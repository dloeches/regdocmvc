﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegDocMVC.Models
{
    public partial class PaiInform
    {
        [Key]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Column("NUM_PAI")]
        [Display(Name = "Número PAI")]
        public double? numPai { get; set; }

        [Column("COD_PAI")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Código PAI")]
        [StringLength(15)]
        public string codPai { get; set; }

        [Column("NOMBRE")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Nombre PAI")]
        [StringLength(50)]
        public string nombre { get; set; }

        [Column("UNID_EJEC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Unidad Ejecución")]
        [StringLength(30)]
        public string unidEjec { get; set; }

        [Column("POBLACION")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Población")]
        [StringLength(50)]
        public string poblacion { get; set; }

        [Column("F_AP_PLICA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. aplica")]
        public DateTime? fApPlica { get; set; }

        [Column("F_PROP_PAI")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Prop")]
        public DateTime? fPropPai { get; set; }

        [Column("AVAL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Aval")]
        public double? aval { get; set; }

        [Column("FEC_AVAL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Aval")]
        public DateTime? fecAval { get; set; }

        [Column("F_CONV_ADM")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Conv. Adm.")]
        public DateTime? fConvAdm { get; set; }

        [Column("GESTION")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Gestión")]
        public string gestion { get; set; }

        [Column("URBANIZA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Urbaniza")]
        public short? urbaniza { get; set; }

        [Column("FEC_DOGV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. DOGV")]
        public DateTime? FecDogv { get; set; }

        [Column("FEC_PRENSA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Prensa")]
        public DateTime? fecPrensa { get; set; }

        [Column("AMPL_PLAZO")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Ampl. Plazo")]
        public short? amplPlazo { get; set; }

        [Column("AV_TIT_CAT")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Tit. Cat.")]
        public DateTime? avTitCat { get; set; }

        [Column("PORC_PERM")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Porc. Perm.")]
        public double? porcPerm { get; set; }

        [Column("REN_APROV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Ren. Aprov.")]
        public short? renAprov { get; set; }

        [Column("PROC_ABREV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Ren. Abrev.")]
        public short? procAbrev { get; set; }

        [Column("NOTARIA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Notaria")]
        [StringLength(35)]
        public string notaria { get; set; }

        [Column("F_ALTER_TEC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Alter. Tec.")]
        public DateTime? fAlterTec { get; set; }

        [Column("ALTER_TEC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Alter Tec")]
        [StringLength(50)]
        public string alterTec { get; set; }

        [Column("HMPG")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "HMPG")]
        public short? hmpg { get; set; }

        [Column("MODIF_PUNT")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "ModifPunt")]
        public short? modifPunt { get; set; }

        [Column("PAI")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Pai")]
        public short? pai { get; set; }

        [Column("PERI")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Peri")]
        public short? peri { get; set; }

        [Column("P_ESPEC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "PEspec")]
        public short? pEspec { get; set; }

        [Column("P_PARCIAL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "PParcial")]
        public short? pParcial { get; set; }

        [Column("ANTEPR_URB")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "AnteprUrb")]
        public short? anteprUrb { get; set; }

        [Column("P_URBANI")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "PUrbani")]
        public short? pUrbani { get; set; }

        [Column("P_REPARC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Preparc")]
        public short? pReparc { get; set; }

        [Column("E_DETALLE")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "E detalle")]
        public short? eDetalle { get; set; }

        [Column("DOGV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Dogv")]
        [StringLength(50)]
        public string dogv { get; set; }

        [Column("F_VTO_DOGV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. VtoDogv")]
        public DateTime? fVtoDogv { get; set; }

        [Column("FEC_BOP")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Bop")]
        public DateTime? fecBop { get; set; }

        [Column("BOP")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "bop")]
        [StringLength(50)]
        public string bop { get; set; }

        [Column("F_JUR_ECO")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. JurEco")]
        public DateTime? fJurEco { get; set; }

        [Column("F_APROB_PLEN")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. AprobPlen")]
        public DateTime? fAprobPlen { get; set; }

        [Column("F_PRO_REP")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. ProRep")]
        public DateTime? fProRep { get; set; }

        [Column("F_REC_PROV")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. RecProv")]
        public DateTime? fRecProv { get; set; }

        [Column("F_REC_DEF")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. RecDef")]
        public DateTime? fRecDef { get; set; }

        [Column("REPARCELA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Reparcela")]
        public short? reparcela { get; set; }

        [Column("IMP_AVALSEC")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Imp. Aval")]
        public decimal? impAvalsec { get; set; }

        [Column("F_DEV_AVAL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. DevAval")]
        public DateTime? fDevAval { get; set; }

        [Column("F_PRORROG1")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Porroga1")]
        public DateTime? fProrrog1 { get; set; }

        [Column("F_VTO_PRO1")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. VtoPro1")]
        public DateTime? fVtoPro1 { get; set; }

        [Column("F_PRORROG2")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Porroga2")]
        public DateTime? fProrrog2 { get; set; }

        [Column("F_VTO_PRO2")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. VtoPro2")]
        public DateTime? fVtoPro2 { get; set; }

        [Column("PROVINCIA")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Provincia")]
        [StringLength(50)]
        public string provincia { get; set; }

        [Column("OBSER")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Observaciones")]
        [StringLength(255)]
        public string obser { get; set; }

        [Column("F_VTO_BOP")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. VtoBop")]
        public DateTime? fVtoBop { get; set; }

        [Column("F_SOL_PRO")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. FSolPro")]
        public DateTime? fSolPro { get; set; }

        [Column("SOL_PRO")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "SolPro")]
        [StringLength(1)]
        public string solPro { get; set; }

        [Column("F_INF_PUB")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. InfPub")]
        public DateTime? fInfPub { get; set; }

        [Column("F_PLIPUBL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Plipubl")]
        public DateTime? fPlipubl { get; set; }

        [Column("F_PLIVTOPUBL")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Plivtopubl")]
        public DateTime? fPlivtopubl { get; set; }

        [Column("F_PLEPUBLPRO")]
        //[Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "F. Plepublpro")]
        public DateTime? fPlepublpro { get; set; }

        [Column("PLE_ADJCOND")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "PleAdjcond")]
        [StringLength(1)]
        public string pleAdjcond { get; set; }

        [Column("F_PLEAPRDEF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "F. Pleaprdef")]
        public DateTime? fPleaprdef { get; set; }

        [Column("F_PLEPUBLDEF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "F. Plepubldef")]
        public DateTime? fPlepubldef { get; set; }

        [Column("F_PLEREG")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "F. Plereg")]
        public DateTime? fPlereg { get; set; }

        [Column("F_PLEDES")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "F. Pledes")]
        public DateTime? fPledes { get; set; }

        [Column("CONC_AVAL")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Conc Aval")]
        [StringLength(200)]
        public string concAval { get; set; }

        [Column("CONC_AMP")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Conc Amp")]
        [StringLength(200)]
        public string concAmp { get; set; }

        [Column("PLEPUBLPRO")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Plepublpro")]
        [StringLength(1)]
        public string plepublpro { get; set; }

        [Column("PLEPUBLDEF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Plepubldef")]
        [StringLength(1)]
        public string plepubldef { get; set; }

        [Column("RETRIB")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Retrib")]
        [StringLength(1)]
        public string retrib { get; set; }

        [Column("F_REPMODIF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepmodif")]
        public DateTime? fRepmodif { get; set; }

        [Column("F_REPCERTREG")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepcertreg")]
        public DateTime? fRepcertreg { get; set; }

        [Column("F_REPNOTTIT")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepnottit")]
        public DateTime? fRepnottit { get; set; }

        [Column("F_REPPRENSA")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepprensa")]
        public DateTime? fRepprensa { get; set; }

        [Column("F_REPDOGV")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepdogv")]
        public DateTime? fRepdogv { get; set; }

        [Column("F_REPVTOINF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepvtoinf")]
        public DateTime? fRepvtoinf { get; set; }

        [Column("F_REPAPROB")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "fRepaprob")]
        public DateTime? fRepaprob { get; set; }

        [Column("F_REPNOTAPROB")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepnotaprob")]
        public DateTime? fRepnotaprob { get; set; }

        [Column("F_REPBOP")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepbop")]
        public DateTime? fRepbop { get; set; }

        [Column("F_REPFIRM")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepfirm")]
        public DateTime? fRepfirm { get; set; }

        [Column("F_REPCERTMUN")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepcertmun")]
        public DateTime? fRepcertmun { get; set; }

        [Column("F_REPPROT")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepprot")]
        public DateTime? fRepprot { get; set; }

        [Column("REPNOTARIA")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Repnotaria")]
        [StringLength(50)]
        public string repnotaria { get; set; }

        [Column("PROTOCOLO")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Protocolo")]
        [StringLength(50)]
        public string protocolo { get; set; }

        [Column("F_REPINSCR")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FRepinscr")]
        public DateTime? fRepinscr { get; set; }

        [Column("REPBOP")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Repbop")]
        [StringLength(50)]
        public string repbop { get; set; }

        [Column("F_URBAPROB")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbaprob")]
        public DateTime? FUrbaprob { get; set; }

        [Column("CEDULA")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Cedula")]
        [StringLength(1)]
        public string cedula { get; set; }

        [Column("F_URBCEDULA")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbcedula")]
        public DateTime? fUrbcedula { get; set; }

        [Column("F_URBACTA")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbacta")]
        public DateTime? fUrbacta { get; set; }

        [Column("F_URBINI")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbini")]
        public DateTime? fUrbini { get; set; }

        [Column("F_URBCERTIF")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbcertif")]
        public DateTime? fUrbcertif { get; set; }

        [Column("F_URBENTR")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "FUrbentr")]
        public DateTime? fUrbentr { get; set; }

        [Column("IMP_AVAL")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Imp. Aval")]
        public decimal? impAval { get; set; }

        [Column("PAGOFIJADO")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Pago fijado")]
        [StringLength(1)]
        public string pagofijado { get; set; }

        [Column("PORCPERMINI")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Porcpermini")]
        public double? porcpermini { get; set; }

        [Column("PORCPERMADH")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Porcpermadh")]
        public double? porcpermadh { get; set; }

        [Column("PORCPERMAYU")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Porcpermayu")]
        public double? porcpermayu { get; set; }

        [Column("PORCPERMOTR")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Porcpermotr")]
        public double? porcpermotr { get; set; }

        [Column("EMPFINAN")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Emp. finan")]
        [StringLength(30)]
        public string empfinan { get; set; }

        [Column("N_SDAD")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "NSdad")]
        public double? nSdad { get; set; }

        [Column("MIGRADO")]
        //[Required(OBSER = "{0} es requerido")]
        [Display(Name = "Migrado")]
        [StringLength(1)]
        public string migrado { get; set; }
    }
}
