﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegDocMVC.Models
{
    public class PaiEntrada
    {
        [Key]
        [Required(ErrorMessage = "{0} es requerido")]
        [Column("N_REG")]
        [Display(Name = "Número Registro")]
        public double nReg { get; set; }

        [Column("F_ENTRADA")]
        [Required(ErrorMessage ="{0} es requerido")]
        [Display(Name = "F. Entrada"), DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[Range(typeof(DateTime), "1/1/1987", "1/1/2050")]
        public DateTime fEntrada { get; set; }

        [Column("COD_PAI")]
        [Required(ErrorMessage = "{0} es requerido")]
        [Display(Name = "Código")]
        [StringLength(12)]
        public string codPai { get; set; }

        [Column("COD_PAI_ANT")]
        [Display(Name = "Código PAI Anterior")]
        [StringLength(12)]
        public string codPaiAnt { get; set; }

        [Column("ASUNTO")]
        [Display(Name = "Asunto")]
        //[StringLength(8000)]
        public string asunto { get; set; }

        [Column("REMITENTE")]
        [Display(Name = "Remitente")]
        [StringLength(255)]
        [MaxLength(255)]
        public string remitente { get; set; }

        [Column("REFERENCIA")]
        [Display(Name = "Referencia")]
        [StringLength(30)]
        [MaxLength(30)]
        public string referencia { get; set; }

        [Column("DESTINATARIO")]
        [Display(Name = "Destinatario")]
        [StringLength(255)]
        [MaxLength(255)]
        public string destinatario { get; set; }

        [Column("PAI_DS_CODDOCU")]
        [StringLength(15)]
        [MaxLength(15)]
        public string paiDsCoddocu { get; set; }

        [Column("AREA_DOC")]
        [Display(Name = "Área")]
        [StringLength(2)]
        public string areaDoc { get; set; }

        [Column("TIPO_DOC")]
        [Display(Name = "Tipo Doc.")]
        [StringLength(1)]
        public string tipoDoc { get; set; }

        [Column("COD_PAI_SAL")]
        [StringLength(12)]
        public string codPaiSal { get; set; }

        [Column("N_REG_SAL")]
        
        public double? nRegSal { get; set; }

        [Column("AUX")]
        
        public double? aux { get; set; }

        [Column("COD_PAI_OLD")]
        [StringLength(12)]
        public string codPaiOld { get; set; }

        [Column("ASUNTO_OLD")]
        [StringLength(255)]
        public string asuntoOld { get; set; }

        [Column("C_PROP_PAI")]
        [Display(Name = "Propietario")]
        public short? cPropPai { get; set; }

        [Column("TIPOLOGIA")]
        [Display(Name = "Tipología")]
        [StringLength(2)]
        public string tipologia { get; set; }

        public string formattedFEntrada => fEntrada.ToShortDateString();
    }
}
