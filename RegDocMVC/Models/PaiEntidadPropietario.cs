﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RegDocMVC.Models
{
    public class PaiEntidadPropietario
    {
        [Key]
        [Required(ErrorMessage = "{0} es requerido")]
        [Column("ID")]
        public int Id { get; set; }

        [Column("NOMBRE")]
        [StringLength(50)]
        public string Nombre { get; set; }
    }
}
