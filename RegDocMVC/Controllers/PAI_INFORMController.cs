﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using RegDocMVC.Models;
using CapaNegocioCore.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace RegDocMVC.Controllers
{
    public class PAI_INFORM : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        private readonly ICombos _configCombos;

        [BindProperty]
        public PaiInform PaiInform{ get; set; }
        public PAI_INFORM(ApplicationDbContext db, IConfiguration config, ICombos configCombo)
        {
            _db = db;
            _config = config;
            _configCombos = configCombo;
        }

        public IActionResult Index()
        {        
            return View();
        }

        public IActionResult GetTipoDocs()
        {
            List<ItemConfig> Lista = _configCombos.getTipoDocs(_config);

            return Json(Lista);
        }

      
        //public JsonResult GetTipoDocDescripcion(int Id)
        //{
        //    return Json(_configCombos.getComboValores(_config, CombosNombres.TIPO_DOC).First(t=>t.Id ==Id));            
        //}

        public IActionResult Upsert(int? Id)
        {
            PaiInform = new PaiInform();
            if (Id == null)
            {
                //create
                return View(PaiInform);
            }
            //update
            PaiInform = _db.PAI_INFORM.FirstOrDefault(u => u.numPai == Id);
            if (PaiInform == null)
            {
                return NotFound();
            }
            return View(PaiInform);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert()
        {
            if (ModelState.IsValid)
            {
                if (PaiInform.numPai == 0)
                {
                    //create
                    _db.PAI_INFORM.Add(PaiInform);
                }
                else
                {
                    _db.PAI_INFORM.Update(PaiInform);
                }
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(PaiInform);
        }

        #region API Calls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            //return Json(new { data = await _db.PAI_ENTRADA.ToListAsync() });
            //var PaisEntrada = await _db.PAI_INFORM.ToListAsync();

            //List<ItemConfig> tiposdoc = _configCombos.getComboValores(_config, CombosNombres.TIPO_DOC);

            //var query = from paiEntrada in PaisEntrada
            //            join tipodoc in tiposdoc
            //                on paiEntrada.tipoDoc equals tipodoc.id into grouping
            //            from tipodoc in grouping.DefaultIfEmpty()
            //            select new
            //            {
            //                paiEntrada.nReg,
            //                paiEntrada.codPai,
            //                paiEntrada.fEntrada,
            //                tipoDocName = tipodoc == null ? String.Empty : tipodoc.nombre
            //            };
            ////select new { paiEntrada, tipodoc };

            //////Left Outer Join
            ////var joinedList = (from pai in PaisEntrada
            ////                  join tipodoc in tiposdoc on pai.tipoDoc equals tipodoc.Id into temp
            ////                  from tipodoc in temp.DefaultIfEmpty()
            ////                  select new
            ////                  {
            ////                     pai.nReg,
            ////                     pai.codPai,
            ////                     pai.fEntrada,
            ////                     TipoDocName = tipodoc == null ? String.Empty : tipodoc.Nombre
            ////                     //Amount = detail == null ? null : detail.Amount
            ////                  });
            //return Json(new { data = query });
            //return Json(query);

            var jsonResult = Json(new { data = await _db.PAI_INFORM.ToListAsync() });
            
            return jsonResult;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var PaiFromDb = await _db.PAI_INFORM.FirstOrDefaultAsync(u => u.numPai  == id);
            if (PaiFromDb == null)
            {
                return Json(new { success = false, message = "Error Borrando" });
            }
            _db.PAI_INFORM.Remove(PaiFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Borrado con exito" });
        }
        #endregion
    }
}