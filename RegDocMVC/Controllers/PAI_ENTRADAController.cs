﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RegDocMVC.Models;
using CapaNegocioCore.Config;

namespace RegDocMVC.Controllers
{
    public class PAI_ENTRADA : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        
        [BindProperty]
        public PaiEntrada PaiEntrada { get; set; }
        public PAI_ENTRADA(ApplicationDbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        public IActionResult Index()
        {
            rellenarCombos();

            return View();
        }

        public IActionResult Upsert(int? Id)
        {
            rellenarCombos();

            PaiEntrada = new PaiEntrada();
            if (Id == null)
            {
                //create
                PaiEntrada.nReg = calcularNreg(PaiEntrada.fEntrada.Year);
                PaiEntrada.codPai = calcularCodPai(PaiEntrada.fEntrada.Year);
                PaiEntrada.fEntrada = DateTime.Today;
                return View(PaiEntrada);
            }
            //update
            PaiEntrada = _db.PAI_ENTRADA.FirstOrDefault(u => u.nReg == Id);

            if (PaiEntrada == null)
            {
                return NotFound();
            }

            return View(PaiEntrada);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert()
        {
            if (ModelState.IsValid)
            {
                if (PaiEntrada.nReg == 0)
                {
                    //create
                    PaiEntrada.nReg = calcularNreg(PaiEntrada.fEntrada.Year);
                    PaiEntrada.codPai = calcularCodPai(PaiEntrada.fEntrada.Year);
                    PaiEntrada.paiDsCoddocu = PaiEntrada.codPai;

                    _db.PAI_ENTRADA.Add(PaiEntrada);
                }
                else
                {
                    _db.PAI_ENTRADA.Update(PaiEntrada);
                }
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(PaiEntrada);
        }

        private double calcularNreg(double anyoEntrada)
        {
            var MaxNReg =  _db.PAI_ENTRADA.Max(u => u.nReg);
            var MaxRegTxt = Convert.ToString(MaxNReg);
            var AnyoNReg = Convert.ToInt32(MaxRegTxt.Substring(0, 4));
            var MaxRegAnyo = Convert.ToDouble(AnyoNReg.ToString() + "999999"); 

            if (AnyoNReg >= anyoEntrada)
            {
                if (MaxRegTxt.Length < 9)
                {
                    var secuencia = Convert.ToDouble(MaxRegTxt.Substring(4));
                    MaxNReg = Convert.ToDouble(AnyoNReg + secuencia.ToString("000000")); // Pasando al Nuevo formato
                }

                if (MaxNReg < MaxRegAnyo)
                {
                    MaxNReg++;
                }
                else
                {
                    //'Reinicia y empieza en 1
                    MaxNReg = Convert.ToDouble((AnyoNReg+1).ToString() + "000001");
                }
            }
            else //if (AnyoNReg < AnyoEntrada)
            {
                //'Reinicia y empieza en 1
                MaxNReg = Convert.ToDouble(anyoEntrada.ToString() + "000001");
            }

            return MaxNReg;
        }

        private string calcularCodPai(double anyoEntrada)
        {
            var MaxNReg = _db.PAI_ENTRADA.Max(u => u.nReg);
            var AnyoNReg = Convert.ToInt32(Convert.ToString(MaxNReg).Substring(0, 4));

            var PaiFromDb =  _db.PAI_ENTRADA.FirstOrDefaultAsync(u => u.nReg == MaxNReg);
            if (PaiFromDb != null)
            {
                if (AnyoNReg >= anyoEntrada)
                {
                    var num = Convert.ToDouble(PaiFromDb.Result.codPai.Substring(1));
                    if (num < 999999)
                        return "R" + (num + 1).ToString("000000");
                }
            }

            return "R000001";
        }

       private void rellenarCombos()
        {
            var itemVacio = new ItemConfig { id = string.Empty, nombre = string.Empty };

            ViewBag.ListPropietarios = (from i in _db.PAI_PROPENTIDAD select new ItemConfig { id = i.Id.ToString(), nombre = i.Nombre }).ToList();
            ViewBag.ListaTipoDocs = (from i in _db.PAI_TIPODOC select new ItemConfig { id = i.Id.ToString(), nombre = i.Nombre }).ToList();
            ViewBag.ListaAreaDocs = (from i in _db.PAI_AREADOC select new ItemConfig { id = i.Id.ToString(), nombre = i.Nombre }).ToList();
            ViewBag.ListaTipologia = (from i in _db.PAI_TIPOLOGIA select new ItemConfig { id = i.Id.ToString(), nombre = i.Nombre }).ToList();

            ViewBag.ListPropietarios.Insert(0, itemVacio);
            ViewBag.ListaTipoDocs.Insert(0, itemVacio);
            ViewBag.ListaAreaDocs.Insert(0, itemVacio);
            ViewBag.ListaTipologia.Insert(0, itemVacio);
        }
        #region API Calls

        [HttpPost]
        public IActionResult GetRegistrosEntrada()
        {

            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumnIndex = Request.Form["order[0][column]"].FirstOrDefault();
            //var sortColumn1 = Request.Form["columns[1][name]"].FirstOrDefault();
            //var sortColumn = Request.Form["columns[" + sortColumnIndex  + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int recordsFiltered = 0;
            string sortColumn = "nReg";

            //find search columns info
            var scodigo = Request.Form["columns[0][search][value]"].FirstOrDefault();
            var snreg= Request.Form["columns[1][search][value]"].FirstOrDefault();
            var sfEntradaIni = Request.Form["columns[2][search][value]"].FirstOrDefault();
            var spropietario = Request.Form["columns[3][search][value]"].FirstOrDefault();
            var sareadoc = Request.Form["columns[4][search][value]"].FirstOrDefault();
            var stipodoc = Request.Form["columns[5][search][value]"].FirstOrDefault();
            var sreferencia = Request.Form["columns[6][search][value]"].FirstOrDefault();
            var stipologia = Request.Form["columns[7][search][value]"].FirstOrDefault();
            var sfEntradaFin = Request.Form["columns[8][search][value]"].FirstOrDefault();

            DateTime fromDate = DateTime.MinValue;
            DateTime toDate = DateTime.MaxValue;

            int ipropietario = !string.IsNullOrEmpty(spropietario) ? Convert.ToInt32(spropietario) : 0;
            double dnreg = !string.IsNullOrEmpty(snreg) ? Convert.ToDouble(snreg) : 0;

            fromDate = DateTime.TryParse(sfEntradaIni, out fromDate) ? Convert.ToDateTime(sfEntradaIni) : DateTime.MinValue;
            toDate = DateTime.TryParse(sfEntradaFin, out fromDate) ? Convert.ToDateTime(sfEntradaFin) : DateTime.MaxValue;

            var PaisEntradaData = from paiEntrada in _db.PAI_ENTRADA
                                       from tipodoc in _db.PAI_TIPODOC.Where(t => Convert.ToString(t.Id)  == paiEntrada.tipoDoc).DefaultIfEmpty()
                                       from areadoc in _db.PAI_AREADOC.Where(a => Convert.ToString(a.Id) == paiEntrada.areaDoc).DefaultIfEmpty()
                                       from prop in _db.PAI_PROPENTIDAD.Where(p => Convert.ToInt16(p.Id) == paiEntrada.cPropPai).DefaultIfEmpty()
                                       from tipologia in _db.PAI_TIPOLOGIA.Where(ti => Convert.ToString(ti.Id) == paiEntrada.tipologia).DefaultIfEmpty()
                                       select new 
                                       {
                                        paiEntrada.nReg,
                                        paiEntrada.codPai,
                                        paiEntrada.fEntrada,
                                        paiEntrada.referencia,
                                        paiEntrada.cPropPai,
                                        propPai   = prop == null ? String.Empty : prop.Nombre,
                                        areaDocId=paiEntrada.areaDoc,
                                        areaDoc = areadoc == null ? String.Empty : areadoc.Nombre,
                                        tipoDocId = paiEntrada.tipoDoc,
                                        tipoDoc =  tipodoc == null ? String.Empty : tipodoc.Nombre ,
                                        tipologiaId = paiEntrada.tipologia,
                                        tipologia = tipologia == null ? String.Empty : tipologia.Nombre
                                       };

            recordsTotal = PaisEntradaData.Count();

            if (!string.IsNullOrEmpty(searchValue))
            {
                PaisEntradaData = PaisEntradaData.Where(m => m.codPai.Contains(searchValue)
                                || Convert.ToString(m.nReg).Contains(searchValue)
                                || Convert.ToString(m.fEntrada).Contains(searchValue)
                                || m.propPai.Contains(searchValue)
                                || m.areaDoc.Contains(searchValue)
                                || m.tipoDoc.Contains(searchValue)
                                || m.tipologia.Contains(searchValue)
                                || m.referencia.Contains(searchValue));
            }

            if (!string.IsNullOrEmpty(scodigo) || !string.IsNullOrEmpty(snreg) ||
                !string.IsNullOrEmpty(sfEntradaIni) || !string.IsNullOrEmpty(sfEntradaFin) ||
                !string.IsNullOrEmpty(spropietario) || !string.IsNullOrEmpty(sreferencia) ||
                !string.IsNullOrEmpty(sareadoc) || !string.IsNullOrEmpty(stipodoc) || !string.IsNullOrEmpty(stipologia))
            {


                PaisEntradaData = PaisEntradaData.Where(c => (scodigo == "" || c.codPai.ToLower().Contains(scodigo.ToLower()))
                                                        &&
                                                        (dnreg == 0 || dnreg == c.nReg)
                                                        &&
                                                        (sareadoc == "" || c.areaDocId.ToLower() == sareadoc.ToLower())
                                                        &&
                                                        (stipodoc == "" || c.tipoDocId.ToLower() == stipodoc.ToLower())
                                                        &&
                                                        (stipologia == "" || c.tipologiaId.ToLower() == stipologia.ToLower())
                                                        &&
                                                        (sreferencia == "" || c.referencia.ToLower() == sreferencia.ToLower())
                                                        &&
                                                        (ipropietario == 0 || ipropietario == c.cPropPai)
                                                        &&
                                                        (fromDate == DateTime.MinValue || c.fEntrada >= fromDate)
                                                        &&
                                                        (toDate == DateTime.MaxValue || c.fEntrada <= toDate)
                                                        );
                /*
                PaisEntradaData = PaisEntradaData.Where(c => (dnreg == 0 || dnreg == c.nReg) 
                                       &&
                                        (scodigo == "" || c.codPai.ToLower().Contains(scodigo.ToLower()))
                                        &&
                                        (ipropietario == 0 || ipropietario == c.cPropPai)
                                        &&
                                        (sreferencia == "" || c.referencia.ToLower() == sreferencia.ToLower())
                                        &&
                                        ((fromDate == DateTime.MinValue || c.fEntrada >= fromDate)
                                        &&
                                        (toDate == DateTime.MaxValue || c.fEntrada <= toDate))
                                        &&
                                        (sareadoc == "" || c.areaDocId.ToLower() == sareadoc.ToLower())
                                       &&
                                        (stipodoc == "" || c.tipoDocId.ToLower() == stipodoc.ToLower())
                                        &&
                                        (stipologia == "" || c.tipologiaId.ToLower() == stipologia.ToLower())
                                    );*/
            }

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                PaisEntradaData = PaisEntradaData.OrderBy(sortColumn + " " + sortColumnDirection);
            }

              
           recordsFiltered = PaisEntradaData.Count();
            if (pageSize ==-1)
            {
                pageSize = recordsFiltered;
            }
           var data = PaisEntradaData.Skip(skip).Take(pageSize).ToList();


           var jsonData = new { draw = draw, recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = data };
           return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpGet]
        //public async Task<IActionResult> GetAll()
        //{
        //    //return Json(new { data = await _db.PAI_ENTRADA.ToListAsync() });
        //    var PaisEntrada = await _db.PAI_ENTRADA.ToListAsync();

        //    List<ItemConfig> tiposdoc = _configCombos.getTipoDocs(_config);
        //    List<ItemConfig> areasdoc = _configCombos.getAreaDocs(_config);
        //    List<ItemConfig> propietarios = _configCombos.getListaPropietarios(_config);
        //    List<ItemConfig> tipologias = _configCombos.getTipologias(_config);

        //    var query = from paiEntrada in PaisEntrada
        //                join tipodoc in tiposdoc
        //                    on paiEntrada.tipoDoc equals tipodoc.id into temp1
        //                from tipodoc in temp1.DefaultIfEmpty()
        //                join areadoc in areasdoc
        //                   on paiEntrada.areaDoc equals areadoc.id into temp2
        //                from areadoc in temp2.DefaultIfEmpty()
        //                join prop in propietarios
        //                  on paiEntrada.cPropPai.ToString() equals prop.id into temp3
        //                from prop in temp3.DefaultIfEmpty()
        //                join tipologia in tipologias
        //                 on  paiEntrada.tipologia equals tipologia.id into temp4
        //                from tipologia in temp4.DefaultIfEmpty()
        //                select new
        //                {
        //                    paiEntrada.nReg,
        //                    paiEntrada.codPai,
        //                    paiEntrada.codPaiAnt,
        //                    paiEntrada.fEntrada,
        //                    paiEntrada.asunto,
        //                    paiEntrada.remitente,
        //                    paiEntrada.referencia,
        //                    paiEntrada.destinatario,
        //                    paiEntrada.paiDsCoddocu,
        //                    propietarioName = prop == null ? String.Empty : prop.nombre,
        //                    areaDocName = areadoc == null ? String.Empty : areadoc.nombre,
        //                    tipoDocName = tipodoc == null ? String.Empty : tipodoc.nombre,
        //                    tipologiaName = tipologia == null ? String.Empty : tipologia.nombre
        //                };
        //    //select new { paiEntrada, tipodoc };

        //    ////Left Outer Join
        //    //var joinedList = (from pai in PaisEntrada
        //    //                  join tipodoc in tiposdoc on pai.tipoDoc equals tipodoc.Id into temp
        //    //                  from tipodoc in temp.DefaultIfEmpty()
        //    //                  select new
        //    //                  {
        //    //                     pai.nReg,
        //    //                     pai.codPai,
        //    //                     pai.fEntrada,
        //    //                     TipoDocName = tipodoc == null ? String.Empty : tipodoc.Nombre
        //    //                     //Amount = detail == null ? null : detail.Amount
        //    //                  });
        //    return Json(new { data = query });
        //    //return Json(query);

        //    //return Json(new { data = await _db.PAI_ENTRADA.ToListAsync() });
        //}

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var PaiFromDb = await _db.PAI_ENTRADA.FirstOrDefaultAsync(u => u.nReg  == id);
            if (PaiFromDb == null)
            {
                return Json(new { success = false, message = "Error Borrando" });
            }
            _db.PAI_ENTRADA.Remove(PaiFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Borrado con exito" });
        }
        #endregion
    }
}