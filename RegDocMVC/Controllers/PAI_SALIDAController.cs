﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RegDocMVC.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CapaNegocioCore.Config;
using Microsoft.Extensions.Configuration;

namespace RegDocMVC.Controllers
{
    public class PAI_SALIDA : Controller
    {

        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        private readonly ICombos _configCombos;

        [BindProperty]
        public PaiSalida PaiSalida { get; set; }
        public PAI_SALIDA(ApplicationDbContext db, IConfiguration config, ICombos configCombo)
        {
            _db = db;
            _config = config;
            _configCombos = configCombo;
        }

        public IActionResult Index()
        {
            return View();
        }
        //public async Task<IActionResult> Index()
        //{
        //   //return View();
        //    return View(await _db.PAI_SALIDA.ToListAsync());
        //}

        public IActionResult Upsert(int? Id)
        {
            var itemVacio = new ItemConfig { id = string.Empty, nombre = string.Empty };

            ViewBag.ListPropietarios = _configCombos.getListaPropietarios(_config);
            ViewBag.ListaTipoDocs = _configCombos.getTipoDocs(_config);
            ViewBag.ListaAreaDocs = _configCombos.getAreaDocs(_config);
            ViewBag.ListaTipologia = _configCombos.getTipologias(_config);

            ViewBag.ListPropietarios.Insert(0, itemVacio);
            ViewBag.ListaTipoDocs.Insert(0, itemVacio);
            ViewBag.ListaAreaDocs.Insert(0, itemVacio);
            ViewBag.ListaTipologia.Insert(0, itemVacio);

            PaiSalida = new PaiSalida();
            if (Id == null)
            {
                //create
                //PaiSalida.nReg = calcularNreg(PaiSalida.fEntrada.Year);
                //PaiSalida.codPai = calcularCodPai(PaiSalida.fEntrada.Year);
                PaiSalida.fSalida  = DateTime.Today;
                return View(PaiSalida);
            }
            //update
            PaiSalida = _db.PAI_SALIDA.FirstOrDefault(u => u.nReg == Id);
            if (PaiSalida == null)
            {
                return NotFound();
            }
            return View(PaiSalida);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert()
        {
            if (ModelState.IsValid)
            {
                if (PaiSalida.nReg == 0)
                {
                    //create
                    //PaiSalida.nReg = calcularNreg(PaiEntrada.fEntrada.Year);
                    //PaiSalida.codPai = calcularCodPai(PaiEntrada.fEntrada.Year);
                    PaiSalida.paiDsCoddocu = PaiSalida.codPai;
                    _db.PAI_SALIDA.Add(PaiSalida);
                }
                else
                {
                    _db.PAI_SALIDA.Update(PaiSalida);
                }
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(PaiSalida);
        }

        #region API Calls
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var PaisSalida = await _db.PAI_SALIDA.ToListAsync();

            List<ItemConfig> tiposdoc = _configCombos.getTipoDocs(_config);
            List<ItemConfig> areasdoc = _configCombos.getAreaDocs(_config);
            List<ItemConfig> propietarios = _configCombos.getListaPropietarios(_config);
            List<ItemConfig> tipologias = _configCombos.getTipologias(_config);

            var query = from paiSalida in PaisSalida
                        join tipodoc in tiposdoc
                            on paiSalida.tipoDoc equals tipodoc.id into temp1
                        from tipodoc in temp1.DefaultIfEmpty()
                        join areadoc in areasdoc
                           on paiSalida.areaDoc equals areadoc.id into temp2
                        from areadoc in temp2.DefaultIfEmpty()
                        join prop in propietarios
                          on paiSalida.cPropPai.ToString() equals prop.id into temp3
                        from prop in temp3.DefaultIfEmpty()
                        join tipologia in tipologias
                         on paiSalida.tipologia equals tipologia.id into temp4
                        from tipologia in temp4.DefaultIfEmpty()
                        select new
                        {
                            paiSalida.nReg,
                            paiSalida.codPai,
                            paiSalida.codPaiAnt,
                            paiSalida.fSalida,
                            paiSalida.asunto,
                            paiSalida.remitente,
                            paiSalida.referencia,
                            paiSalida.destinatario,
                            paiSalida.paiDsCoddocu,
                            propietarioName = prop == null ? String.Empty : prop.nombre,
                            areaDocName = areadoc == null ? String.Empty : areadoc.nombre,
                            tipoDocName = tipodoc == null ? String.Empty : tipodoc.nombre,
                            tipologiaName = tipologia == null ? String.Empty : tipologia.nombre
                        };
           
            return Json(new { data = query });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var PaiFromDb = await _db.PAI_SALIDA.FirstOrDefaultAsync(u => u.nReg  == id);
            if (PaiFromDb == null)
            {
                return Json(new { success = false, message = "Error Borrando" });
            }
            _db.PAI_SALIDA.Remove(PaiFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Borrado con exito" });
        }
        #endregion
    }
}