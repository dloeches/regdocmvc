﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapaNegocioCore.Config
{
    public class ItemConfig
    {
        public string id { get; set; }
        public string nombre { get; set; }
    }
}
