﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapaNegocioCore.Config
{
    public class Combos: ICombos
    {
        private List<ItemConfig> getComboValores(IConfiguration Config ,string configSection)
        {

            var testUsers = Config.GetSection(configSection)
          .GetChildren()
          .ToList()
          .Select(x =>
              (
              x.GetValue<string>("Id"),
              x.GetValue<string>("Nombre")
              )
          ).ToList();


            List<ItemConfig> Lista = Lista = testUsers.ConvertAll(x => new ItemConfig { id = x.Item1, nombre = x.Item2 });

            return Lista;
        }

        public List<ItemConfig> getListaPropietarios(IConfiguration Config)
        {
            return getComboValores(Config, ComboNombres.PROPIETARIO);
        }

        public List<ItemConfig> getTipoDocs(IConfiguration Config)
        {
            return getComboValores(Config, ComboNombres.TIPO_DOC);
        }

        public List<ItemConfig> getAreaDocs(IConfiguration Config)
        {
            return getComboValores(Config, ComboNombres.AREA_DOC);
        }

        public List<ItemConfig> getTipologias(IConfiguration Config)
        {
            return getComboValores(Config, ComboNombres.TIPOLOGIA);
        }
    }
}