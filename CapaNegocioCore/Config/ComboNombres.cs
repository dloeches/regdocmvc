﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapaNegocioCore.Config
{
    public static class ComboNombres
    {
        public const string AREA_DOC = "AreaDoc";
        public const string TIPO_DOC = "TipoDoc";
        public const string PROPIETARIO = "CodPropietario";
        public const string TIPOLOGIA = "Tipologia";
    }
}