﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapaNegocioCore.Config
{
    public interface ICombos
    {
        //List<ItemConfig> getComboValores(IConfiguration Config, string configSection);
        List<ItemConfig> getListaPropietarios(IConfiguration Config);
        List<ItemConfig> getTipoDocs(IConfiguration Config);
        List<ItemConfig> getAreaDocs(IConfiguration Config);
        List<ItemConfig> getTipologias(IConfiguration Config);    
    }
}